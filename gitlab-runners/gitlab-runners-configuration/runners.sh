#!/bin/bash

docker_runner_name="gitlab-runner"
docker_runner_count=3
docker_runner_config="config.toml"

for i in $(eval echo "{1..$docker_runner_count}")
do
  service="$docker_runner_name$i"
  echo "[+] Registering runner $service"
  docker exec -it $service \
  gitlab-runner register \
    --non-interactive \
    --url "${GITLAB_URL}" \
    --registration-token "${GITLAB_REGISTRATION_TOKEN}" \
    --executor "${GITLAB_EXECUTOR}" \
    --docker-image "${GITLAB_DOCKER_IMAGE}" \
    --description "$service-${GITLAB_DESCRIPTION}" \
    --maintenance-note "$service-${GITLAB_MAINTENANCE_NOTE}" \
    --tag-list "${GITLAB_TAG_LIST}" \
    --run-untagged="${GITLAB_RUN_UNTAGGED}"
  docker exec -it $service \
        gitlab-runner verify && gitlab-runner start
done
