def test_addition():
    numbers = [1,2,3,4]
    assert sum(numbers) == 10

if __name__ == "__main__":
    test_addition()
    print("Tests passed!")
