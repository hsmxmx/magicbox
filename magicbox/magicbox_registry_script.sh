#/bin/bash
source .env

# TBD - Not tested yet
echo "[+] Installing docker"

# 1 - Set up repository
# 1.1 - Update the apt package index and install packages to allow apt to use a repository over HTTPS:
apt-get update
apt-get -y install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

# 1.2 - Add Docker’s official GPG key:
mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg

# 1.3 - Use the following command to set up the repository:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

# 2 - Install Docker Engine
# 2.1 - Update the apt package index:
apt-get update

# 2.2 - Troubleshooting - Receiving a GPG error when running apt-get update:
# chmod a+r /etc/apt/keyrings/docker.gpg
# apt-get update

# 3 - Install Docker Engine, containerd, and Docker Compose.
apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin

# 4 - Local Registry / Manage gitlab or any other service
echo "[+] Creating local docker-registry"
# echo "insecure-registries":["172.20.0.2:5000"] > /etc/docker/daemon.json
# service docker restart
cd /app/ && docker build . -t magicbox
docker login localhost:5000; $DOCKER_USER; $DOCKER_PASS
docker image tag magicbox localhost:5000/magicbox
docker push localhost:5000/magicbox