import requests
from PIL import Image, ImageDraw
from copy import copy
import imgkit
from os import path, remove
import pathlib
# sudo apt-get install wkhtmltopdf

class weatherBot:
    # Depends on supported granularity of API - please see https://www.weatherbit.io/api
    # Currently supported:
    # Granularity of the API - Options: ['daily','hourly','subhourly']
    # History: daily, hourly, subhourly
    # Forecast: daily, hourly
    # Air quality: hourly
    # Will only affect forecast requests.
    def __init__(self, current_location, city, granularity, history, air_quality):
        self.current_location = current_location
        self.city = city
        self.granularity = granularity
        self.history = history
        self.air_quality = air_quality
        
    def __init__(self):
        self.current_location = None
        self.city = None
        self.granularity = None
        self.history = None
        self.air_quality = None
        
    def __repr__(self) -> str:
        return """{type(self).__name__} \n (current_location={self.current_location} \n
                    city={self.city} \n
                    granularity={self.granularity} \n
                    history={self.history} \n
                    air_quality={self.air_quality}"""
    
    
    # Currrent Location
    def __set_current_location__(self, current_location):
        self.current_location = current_location
        
    def __get_current_location__(self):
        current_location_copy = copy(self.current_location)
        return current_location_copy
    
    # City
    def __set_city__(self, city):
        self.city = city
    
    def __get_city__(self) -> str:
        city_copy = copy(self.city)
        return city_copy
    
    # Granularity
    def __set_granularity__(self, granularity):
        self.granularity = granularity
    
    def __get_granularity__(self) -> str:
        granularity_copy = copy(self.granularity)
        return granularity_copy
    
    # History
    def __set_history__(self, history):
        self.history = history
        
    def __get_history__(self) -> str:
        history_copy = copy(self.history)
        return history_copy
    
    # Air Quality
    def __set_air_quality__(self, air_quality):
        self.air_quality = air_quality
        
    def __get_air_quality__(self) -> str:
        air_quality_copy = copy(self.air_quality)
        return air_quality_copy
        
    # Bot Description
    def get_description(self) -> str:
        return """
        Granularity: This parameter specifies the level of detail for the weather data. Possible values are:
            -   daily: provides daily weather data.
            -   hourly: provides hourly weather data.
            -   subhourly: provides weather data at a higher frequency than hourly, such as every 15 or 30 minutes.
        
        History: This parameter allows you to retrieve past weather data. Possible values are:
            -   daily: provides daily weather data from the past.
            -   hourly: provides hourly weather data from the past.
            -   subhourly: provides weather data from the past at a higher frequency than hourly, such as every 15 or 30 minutes.
        
        Forecast: This parameter allows you to retrieve future weather predictions. Possible values are:
            -   daily: provides daily weather forecasts.
            -   hourly: provides hourly weather forecasts.
        
        Air quality: This parameter allows you to retrieve air quality data. Possible values are:
            -   hourly: provides hourly air quality data.
        """
    
    def forecast_by_city(self, city):
        # Passing the City name to the url
        url = 'https://wttr.in/{}'.format(city)
        image_path = str(pathlib.Path(__file__).parent.resolve()) + '\\images\\result.png'
        if path.exists(image_path):
            remove(image_path)
        imgkit.from_url(url, image_path)
        img = open(image_path, 'rb')
        return img

        
        
        

""" 
lat = 35.50
lon = -78.50

api = Api(api_key)

### FORECASTS

# Set the granularity of the API - Options: ['daily','hourly','subhourly']
# Depends on supported granularity of API - please see https://www.weatherbit.io/api
# Currently supported:
# History: daily, hourly, subhourly
# Forecast: daily, hourly
# Air quality: hourly
# Will only affect forecast requests.
api.set_granularity('daily')


### Forecasts (daily)

# Query by lat/lon - get extended forecast out to 240 hours (default 48 hours)
forecast = api.get_forecast(lat=lat, lon=lon, hours=240)

# You can also query by city:
forecast = api.get_forecast(city="Raleigh,NC", hours=240)

# Or City, state, and country:
forecast = api.get_forecast(city="Raleigh", state="North Carolina", country="US", hours=240)

# Or Postal code:
forecast = api.get_forecast(postal_code="27601", country="US", hours=240)

# get_series requires a list of fields to return in a time series (list).
# See documentation for field names: https://www.weatherbit.io/api
print(forecast.get_series(['high_temp','low_temp','precip','weather']))


### Forecasts (hourly)
api.set_granularity('hourly')


# Query by lat/lon - get extended forecast out to 240 hours (default 48 hours)
forecast = api.get_forecast(lat=lat, lon=lon, hours=240)

# Or Postal code:
forecast = api.get_forecast(postal_code="27601", country="US", hours=240)

# get an hourly forecast:
print(forecast.get_series(['temp','precip','weather', 'solar_rad']))


### Forecasts (hourly - Air quality)
# Get an hourly air quality forecast for a lat/lon
forecast_AQ = api.get_forecast(source='airquality', lat=lat, lon=lon)
print(forecast_AQ.get_series(['aqi','pm10','no2']))


### HISTORY

# Get sub-hourly history by lat/lon:
api.set_granularity('subhourly')
history = api.get_history(lat=lat, lon=lon, start_date='2018-02-01',end_date='2018-02-02')

# To get a daily time series of temperature, precipitation, and rh:
print history.get_series(['precip','temp','rh'])

# Get hourly history by lat/lon
api.set_granularity('hourly')
history = api.get_history(lat=lat, lon=lon, start_date='2018-02-01',end_date='2018-02-02')

# To get an hourly time series for these fields:
print(history.get_series(['precip','temp','rh','solar_rad']))

# Get historical air quality data
history_AQ = api.get_history(source='airquality', lat=lat, lon=lon, start_date='2018-02-01',end_date='2018-02-02')
print(history_AQ.get_series(['aqi','pm10','no2']))


### Current Conditions

# Get current air quality
AQ = api.get_current(source='airquality', city="Raleigh", state="North Carolina", country="US")
print(AQ.get(['aqi','pm10','pm25']))

# Get current conditions
current_weather = api.get_current(city="Raleigh", state="North Carolina", country="US")
print(current_weather.get(['weather','temp','precip']))
"""
