import copy
import weatherBot

class magicboxBot:
    def __init__(self, weather_bot):
        self.weather_bot = weather_bot
        
    # WeatherBot
    def __set_weather_bot__(self, weather_bot):
        self.weather_bot = weather_bot
    
    def __get_weather_bot__(self) -> weatherBot:
        weather_bot_copy = copy.copy(self.weather_bot)
        return weather_bot_copy
    
    # ActualBot
    def __set_actual_bot__(self, actual_bot):
        self.actual_bot = actual_bot
    
    # TBD -> Global Bot Manager
    def __get_actual_bot__(self):
        actual_bot_copy = copy.copy(self.actual_bot)
        return actual_bot_copy
        
        
    def __repr__(self) -> str:
        return """{type(self).__name__} \n (weather_bot={self.weather_bot} \n
                    actual_bot={self.actual_bot}"""