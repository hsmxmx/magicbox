import logging
import os
from magicboxBot import magicboxBot
from weatherBot import weatherBot
from telegram import Update, InlineKeyboardMarkup, InlineKeyboardButton, Bot, BotCommand
from telegram.ext import filters, ApplicationBuilder, ContextTypes, CommandHandler, MessageHandler, CallbackQueryHandler, CallbackContext, Application
from uuid import uuid4
from dotenv import load_dotenv

load_dotenv()
GCP_PROJECT_ID = os.getenv('TELEGRAM_TOKEN')

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
        await context.bot.send_message(chat_id=update.effective_chat.id, text="I'm a bot, please talk to me!")
    
    
async def weather(update: Update, context: ContextTypes.DEFAULT_TYPE):
    print(context.args)
    city = context.args
    weather_capture = weather_bot.forecast_by_city(city)
    await update.message.reply_photo(photo=weather_capture)
    # description = weather_bot.get_description()
    # await context.bot.send_message(chat_id=update.effective_chat.id, text=description)
    # keyboard = [
    #        [InlineKeyboardButton("🏙️ City", callback_data='weatherbot_city')],
    #        [InlineKeyboardButton("🗺️ Country", callback_data='weatherbot_country')]
    #    ]
    # reply_markup = InlineKeyboardMarkup(keyboard)
    # await context.bot.send_message(chat_id=update.effective_chat.id, text="Choose an option:", reply_markup=reply_markup)

    
async def button(update: Update, context: CallbackContext) -> None:
    """Parses the CallbackQuery and updates the message text."""
    query = update.callback_query
    # CallbackQueries need to be answered, even if no notification to the user is needed
    # Some clients may have trouble otherwise. See https://core.telegram.org/bots/api#callbackquery
    await query.answer()
    if query.data == 'weatherbot_city':
        await context.bot.send_message(chat_id=update.effective_chat.id, text="🏙️ Choose your city:")
        print(context.args)
        

    await query.edit_message_text(text="Selected option: {%s}" % query.data)    
    
async def post_init(application: Application) -> None:
    command_list = [BotCommand("start", "Here starts your application!"),
                    BotCommand("help", "Asking for help?"),
                    BotCommand("weather", "How is this week weather?")]
    await application.bot.set_my_commands(command_list)

def main():
    application = ApplicationBuilder().token(GCP_PROJECT_ID).post_init(post_init=post_init).build()
    
    # Command handler
    application.add_handler(CommandHandler('start', start))

    # Message handler
    application.add_handler(CommandHandler('weather', weather))

    # Handler for callbacks
    application.add_handler(CallbackQueryHandler(button))
    
    # Initialize and start app by polling updates from Telegram and a graceful shutdown of the app exit
    # The app will shut down when KeyboardInterrupt or SystemExit is raised
    application.run_polling()

    
if __name__ == '__main__':
    weather_bot = weatherBot()
    magicbox_bot = magicboxBot(weather_bot)
    main()




"""
    application.add_handler(CommandHandler('echo', echo))
    application.add_handler(CommandHandler('caps', caps))
    application.add_handler(CommandHandler('put', put))
    application.add_handler(CommandHandler('get', get))
    
    application.add_handler(MessageHandler(filters.TEXT & (~filters.COMMAND), echo))

async def caps(update: Update, context: ContextTypes.DEFAULT_TYPE):
    text_caps = ' '.join(context.args).upper()
    await context.bot.send_message(chat_id=update.effective_chat.id, text=text_caps)


async def put(update, context):
    Usage: /put value
    # Generate ID and separate value from command
    key = str(uuid4())
    # We don't use context.args here, because the value may contain whitespaces
    value = update.message.text.partition(' ')[2]

    # Store value
    context.user_data[key] = value
    # Send the key to the user
    await update.message.reply_text(key)

async def get(update, context):
    Usage: /get uuid
    # Separate ID from command
    key = context.args[0]

    # Load value and send it to the user
    value = context.user_data.get(key, 'Not found')
    await update.message.reply_text(value)
    
async def echo(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(chat_id=update.effective_chat.id, text=update.message.text)
"""