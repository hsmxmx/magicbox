# MagicBox
## Getting started with magic
Welcome to MagicBox, a Telegram bot designed to provide real-time weather information for any location in the world. This bot is built using [Python](https://www.python.org/) and the [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot) library, making it easy to create and configure.

MagicBox uses the Telegram API to allow users to request weather information for a specific location by simply sending the bot a message with the location. The bot then uses the [wttr](https://wttr.in/) API to retrieve the current weather data for the requested location and sends it back to the user in an easy-to-read format.

The project also utilizes [GitLab's CI/CD](https://docs.gitlab.com/ee/ci/) (Continuous Integration and Continuous Deployment) technology, making it easy to test and deploy the application. This includes the use of local runners defined in docker-compose, which allows for testing and building the application in a controlled environment. By using GitLab's built-in CI/CD pipeline, we can ensure that the application is always in a releasable state and that new features can be quickly and easily deployed to production.

Additionally, the project is built using [Docker](https://www.docker.com/) and [Docker-compose](https://docs.docker.com/compose/gettingstarted/), providing the ability to easily deploy the application locally in case a PaaS such as [Heroku](https://www.heroku.com/) is not available. The configuration for this is defined in the Procfile and .gitlab-ci.yml files. By using these technologies, we can ensure that the application is running in a consistent environment, regardless of where it is deployed.

Overall, MagicBox is an easy-to-use and highly configurable bot that can provide you with real-time weather information for any location in the world. Whether you're a traveler planning a trip, or just want to know what the weather is like in your hometown, MagicBox has got you covered.

To get started, simply install Telegram and search for the MagicBox bot. Once you've found it, send the bot a message with the location for which you would like to receive weather information. The bot will quickly respond with the current weather data for that location.

## Installation - Docker
### Clone the repository
1. Go to the URL https://gitlab.com/hsmxmx/magicbox and clone the project to your local machine.
```
git clone https://gitlab.com/hsmxmx/magicbox.git
```

### Install docker and docker-compose
2. Go to [how to install Docker and docker-compose](https://docs.docker.com/compose/install/) and start installing it in your machine

### Compose up your project
3. Navigate to /magicbox/ and compose up the project:
```
docker-compose up --build --no-cache -d
```

### Compose down your project
4. Navigate to /magicbox/ and compose up the project:
```
docker-compose down
```

## Installation - Local Host
### Clone the repository
1. Go to the URL https://gitlab.com/hsmxmx/magicbox and clone the project to your local machine.
```
git clone https://gitlab.com/hsmxmx/magicbox.git
```

### Create a virtual environment
2. Create a new virtual environment for the project using virtualenv or venv.
```
python3 -m venv myenv
```

### Activate the virtual environment
3. Once the new virtual environment has been created, enable it to start using the configuration provided.
```
source myenv/bin/activate
```

### Install the dependencies
4. Install the dependencies specified in the requirements.txt file by running the following command:
```
pip install -r requirements.txt
```

### Create a .env file
5. Create a new file in the root of the project called .env and add the environment variables required for the project.
```
TELEGRAM_TOKEN=YOUR_TOKEN
```

### Run the application
6. Run the application using the following command:
```
python3 magicbox.py
```
Once the application is running, you should be able to interact with the bot via Telegram.

### Stop the application
7. To stop the application, press CTRL+C.

### Clean up
8. When you are done testing the application, deactivate the virtual environment:
```
deactivate
```
And remove the virtual environment
```
rm -rf myenv
```

## Integration with Heroku
### Create a Heroku app
1. Go to [Heroku website](https://heroku.com)and create a new app.
Make note of the app name and the URL.

### Add the Heroku remote
2. In your local repository, add the Heroku remote by running the following command:
```
heroku git:remote -a <your-app-name>
```

### Add environment variables
3. Go to your GitLab project's settings and click on 'CI/CD'
Click on 'Expand' under 'Variables'
Add the following environment variables:
```
HEROKU_APP_NAME
HEROKU_API_KEY
```

### Commit and push the changes
4. Commit and push the changes to your repository.

### Check the CI/CD pipeline
5. Go to the GitLab CI/CD pipelines page and check that the pipeline passed successfully.
Once the pipeline is passed, you should see the changes in your Heroku app.

### Test the application
Go to the Heroku app URL and test the application.

### Notes
Make sure the gitlab-ci.yml and Procfile files are correctly configured and that they match the requirements of the project and the Heroku app.

## Roadmap
New Ideas and Roadmap for Adding a New Feature/Bot and Enabling SSH Configuration:

### Create a new feature/bot:
- [ ] Research and identify the specific use case and requirements for the new feature/bot.
- [ ] Design the overall architecture and flow of the feature/bot.
- [ ] Create a new branch for the feature/bot development and begin coding.
- [ ] Test the feature/bot thoroughly to ensure it is functioning as intended.
- [ ] Integrate the feature/bot with the existing codebase and test again to ensure compatibility.
- [ ] Create and submit a merge request for review and testing by the development team.
- [ ] Once the feature/bot has been approved and tested, merge it into the master branch and deploy to production.

### Enable SSH Configuration:
- [ ] Research and identify the specific requirements for enabling SSH configuration.
- [ ] Create a new branch for the SSH configuration development and begin coding.
- [ ] Create a script that automates the process of enabling SSH configuration on the target machines.
- [ ] Test the script thoroughly to ensure it is functioning as intended.
- [ ] Integrate the script with the existing codebase and test again to ensure compatibility.
- [ ] Create and submit a merge request for review and testing by the development team.
- [ ] Once the SSH configuration has been approved and tested, merge it into the master branch and deploy to production.
- [ ] Document the process for future reference, including information about the machines, users and keys involved in the SSH configuration.

## Support
The [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot) is dedicated to the development and maintenance of the asynchronous python library interface for the Telegram Bot API. This library allows for easy integration of Telegram bots with Python applications, providing a convenient and user-friendly way to interact with the Telegram Bot API. If you have any questions or issues related to the python-telegram-bot library, please feel free to open an issue or merge request in this group.

Special thanks to [@igor_chubin](https://twitter.com/intent/follow?original_referer=https%3A%2F%2Fwttr.in%2F&ref_src=twsrc%5Etfw%7Ctwcamp%5Ebuttonembed%7Ctwterm%5Efollow%7Ctwgr%5Eigor_chubin&region=follow_link&screen_name=igor_chubin) for the development and maintenance of the Weather API. This API allows developers to easily retrieve current weather data for any location in the world by simply specifying the location. 

## References
- [How to deploy python project in gitlab ci/cd with Heroku](https://www.accordbox.com/blog/how-deploy-python-project-heroku-gitlab-ci/)
- [Getting started with Python / Heroku](https://devcenter.heroku.com/articles/getting-started-with-python)
- [Gitlab Security](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/secessentialshandson1.html)
- [Docker Registry](https://docs.docker.com/registry/deploying/)
- [Gitlab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/)
- [Gitlab Container Scanner](https://docs.gitlab.com/ee/user/application_security/container_scanning/)
- [Gitlab Runners Registry](https://docs.gitlab.com/runner/register/index.html#linux)
- [Gitlab Runner Autoscale](https://docs.gitlab.com/runner/executors/docker_machine.html)
- [Python Telegram Bot](https://github.com/python-telegram-bot/python-telegram-bot)
- [Python Telegram Bot Documentation](https://python-telegram-bot.org/)